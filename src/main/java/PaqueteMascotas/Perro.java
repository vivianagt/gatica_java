package PaqueteMascotas;

/**
 *
 * @author viviana
 */
public class Perro {

    private String nombre;
    private String peso;
    private String tamaño;
    private String genero;
    private String raza;
    
    public Perro() {
    }

    public Perro(String nombre, String peso, String tamaño, String genero, String raza) {
        this.nombre = nombre;
        this.peso = peso;
        this.tamaño = tamaño;
        this.genero = genero;
        this.raza = raza;
    }
    
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getTamaño() {
        return tamaño;
    }

    public void setTamaño(String tamaño) {
        this.tamaño = tamaño;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }
    
    
}
