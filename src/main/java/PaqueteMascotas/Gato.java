package PaqueteMascotas;

/**
 *
 * @author viviana
 */
public class Gato {

    private int edad;
    private String genero;
    private String tamaño;
    private String raza;
    
    public Gato() {
    }

    public Gato(int edad, String genero, String tamaño, String raza) {
        this.edad = edad;
        this.genero = genero;
        this.tamaño = tamaño;
        this.raza = raza;
    }
    

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getTamaño() {
        return tamaño;
    }

    public void setTamaño(String tamaño) {
        this.tamaño = tamaño;
    }

        
    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

}
