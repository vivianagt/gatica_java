package PaqueteMascotas;

/**
 *
 * @author viviana
 */
public class Hamster {

    private String nombre;
    private String especie;
    private String peso;
    private String tamaño;

    public Hamster() {
    }

    public Hamster(String nombre, String especie, String peso, String tamaño) {
        this.nombre = nombre;
        this.especie = especie;
        this.peso = peso;
        this.tamaño = tamaño;
    }
    
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getTamaño() {
        return tamaño;
    }

    public void setTamaño(String tamaño) {
        this.tamaño = tamaño;
    }
    
    
    
}
